from bs4 import BeautifulSoup  # 网页解析，获取数据
import re  # 正则表达式，进行文字匹配
import urllib.request, urllib.error  # 指定url，获取网页数据
import xlwt  # 进行excel操作

# 步骤：
# 1、爬取网页
# 2、逐一解析数据
# 3、保存数据

# 定义筛选链接的正则表达式
findLink = re.compile(r'<a href="(.*?)"')  # 创造一个正则表达式对象

def main():
    baseurl = "https://movie.douban.com/top250?start="
    # #1、爬取网页
    getData(baseurl)
    # #2、逐一解析数据
    # savaPath = r"\豆瓣电影Top250.xls"
    # #3、保存数据
    # saveData(savaPath)

# 得到指定的一个url的网页的内容
# head用来模拟浏览器头部信息，向豆瓣服务器发送消息
# 用户代理，表示告诉豆瓣服务器，我们是什么类型的机器以及浏览器
# (本质上是告诉浏览器，我们可以接收什么类型的文件内容、我们可以显示什么文件内容）
def askURL(url):
    head = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36"
    }
    request = urllib.request.Request(url, headers=head)
    html = ""
    try:
        response = urllib.request.urlopen(request)
        html = response.read().decode("utf-8")
        # print(html)
    except urllib.error.URLError as e:
        if hasattr(e, "code"):
            print(e.code)
        if hasattr(e, "reason"):
            print(e.reason)
    return html


# 爬取网页
def getData(baseurl):
    # 爬取网页并放在一个list中
    datalist = []
    # 调用获取页面信息，重复10次，每次25条
    # for i in range(1,10):
    i = 0
    for i in range(0, 1):
        url = baseurl + str(i * 25)
        html = askURL(url)
        # 逐一解析数据
        soup = BeautifulSoup(html, "html.parser")  # 解析html对象，用html.parser解析器
        # 查找符合要求的字符串，存储在列表中
        data = []  # 存储电影信息
        for item in soup.find_all('div', class_="item"):  # 查找所有包含class属性且class属性值为item的，名字为div的标签
            item = str(item)
            link = re.findall(findLink, item)[0]  # 用正则表达式查找指定文件
            print(link)
    # 输出结果

    return datalist


# 保存数据
def saveData():
    print()


if __name__ == '__main__':
    main()
